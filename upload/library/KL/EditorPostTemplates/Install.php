<?php

/**
 * KL_EditorPostTemplates_Install
 *
 * @author: katsulynx
 * @last_edit:    19.10.2015
 */
class KL_EditorPostTemplates_Install
{
    /*
     * TYPE: VARIABLE
     * PURPOSE: ADDON TABLES
     * @last_edit: 30.08.2015
     */
    protected static $_tables = array(
        'post_template_user' => array(
            'createQuery' => "
								CREATE TABLE IF NOT EXISTS
								`xf_kl_ept_post_template_user` (
								  `template_id` int(10) NOT NULL AUTO_INCREMENT,
								  `title` VARCHAR(50) NOT NULL,
								  `user_id` int(10) NOT NULL,
								  `template_date` int(10) NOT NULL,
								  `template_edit_date` int(10) NOT NULL,
								  `content` mediumtext NOT NULL,
								  `active` tinyint(1) NOT NULL,
                                  PRIMARY KEY (`template_id`)
								) DEFAULT CHARSET=`utf8` COMMENT='[KL] EditorPostTemplates'
								",
            'dropQuery' => 'DROP TABLE `xf_kl_ept_post_template_user`'
        ),
        'post_template_user_ignore' => array(
            'createQuery' => "
								CREATE TABLE IF NOT EXISTS `xf_kl_ept_post_template_user_ignore` (
                                  `id` int(10) NOT NULL,
								  `user_id` int(10) NOT NULL,
								  `template_id` int(10) NOT NULL,
                                  PRIMARY KEY (`id`)
								) DEFAULT CHARSET=`utf8` COMMENT='[KL] EditorPostTemplates'
								",
            'dropQuery' => 'DROP TABLE `xf_kl_ept_post_template_user_ignore`'
        ),
        'post_template_admin' => array(
            'createQuery' => "
                                CREATE TABLE IF NOT EXISTS `xf_kl_ept_post_template_admin` (
                                  `template_id` int(10) NOT NULL AUTO_INCREMENT,
                                  `title` varchar(50) NOT NULL,
                                  `category_id` int(10) DEFAULT NULL,
                                  `forums` blob,
                                  `resource_manager_categories` blob,
                                  `template_date` int(10) NOT NULL,
                                  `content` mediumtext,
                                  `active` tinyint(1) NOT NULL,
                                  `position` int(10) NOT NULL,
                                  PRIMARY KEY (`template_id`)
                                ) DEFAULT CHARSET=`utf8` COMMENT='[KL] EditorPostTemplates'
								",
            'dropQuery' => 'DROP TABLE `xf_kl_ept_post_template_admin`'
        ),
        'post_template_admin_categories' => array(
            'createQuery' => "
								CREATE TABLE IF NOT EXISTS `xf_kl_ept_post_template_admin_categories` (
                                  `category_id` int(10) NOT NULL AUTO_INCREMENT,
                                  `category_name` varchar(50) NOT NULL,
                                  `position` int(10) NOT NULL,
                                  `parent_category_id` int(10) DEFAULT NULL,
                                  PRIMARY KEY (`category_id`)
                                ) DEFAULT CHARSET=`utf8`  COMMENT='[KL] EditorPostTemplates'
								",
            'dropQuery' => 'DROP TABLE `xf_kl_ept_post_template_admin_categories`'
        )
    );

    /*
     * TYPE: INSTALL
     * PURPOSE: INSTALL/UPGRADE ADDON
     * @last_edit: 12.09.2015
     */
    public static function install()
    {
        /* gather required stuff */
        $db         = XenForo_Application::get('db');
        $addonModel = XenForo_Model::create("XenForo_Model_AddOn");
        $existingAddon = $addonModel->getAddonById('kl_editor_post_templates');

        if(!$existingAddon) {
            foreach (self::$_tables as $table)
                $db->query($table['createQuery']);
        }
        else {
            if(7 > $existingAddon ? ($existingAddon['version_id']) : 7) {
                $db->query('ALTER TABLE `xf_kl_ept_post_template_admin` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;');
                $db->query('ALTER TABLE `xf_kl_ept_post_template_admin_categories` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;');
                $db->query('ALTER TABLE `xf_kl_ept_post_template_user` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;');
                $db->query('ALTER TABLE `xf_kl_ept_post_template_user_ignore` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;');
                $db->query('ALTER TABLE `xf_kl_ept_post_template_user` CHANGE `title` `title` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;');
                $db->query('ALTER TABLE `xf_kl_ept_post_template_user` CHANGE `content` `content` MEDIUMTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;');
                $db->query('ALTER TABLE `xf_kl_ept_post_template_admin` CHANGE `title` `title` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;');
                $db->query('ALTER TABLE `xf_kl_ept_post_template_admin` CHANGE `content` `content` MEDIUMTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;');
            }
        }
    }

    /*
     * TYPE: UNINSTALL
     * PURPOSE: REMOVE ADDON
     * @last_edit: 23.07.2015
     */
    public static function uninstall()
    {
        $db = XenForo_Application::get('db');
        foreach (self::$_tables as $table)
            $db->query($table['dropQuery']);
    }

}