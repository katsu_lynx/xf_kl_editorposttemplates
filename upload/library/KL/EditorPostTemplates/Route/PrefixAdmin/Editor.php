<?php

/**
 * KL_EditorPostTemplates_Route_PrefixAdmin_Editor
 *
 * @author: Nerian
 * @last_edit:    24.07.2015
 */
class KL_EditorPostTemplates_Route_PrefixAdmin_Editor implements XenForo_Route_Interface
{
    /**
     * @param $routePath
     * @param Zend_Controller_Request_Http $request
     * @param XenForo_Router $router
     * @return XenForo_RouteMatch
     */
    public function match($routePath, Zend_Controller_Request_Http $request, XenForo_Router $router)
    {
        $action = $router->resolveActionWithIntegerParam($routePath, $request, 'template_id');

        return $router->getRouteMatch('KL_EditorPostTemplates_ControllerAdmin_Editor', $action, 'kl_ept_templates');
    }

    public function buildLink($originalPrefix, $outputPrefix, $action, $extension, $data, array &$extraParams)
    {
        if (isset($data['template_id'])) {
            return XenForo_Link::buildBasicLinkWithIntegerParam($outputPrefix, $action, $extension, $data, 'template_id', 'title');
        } else {
            return XenForo_Link::buildBasicLinkWithIntegerParam($outputPrefix, $action, $extension, $data, 'category_id', 'category_name');
        }
    }
}