<?php

/**
 * KL_EditorPostTemplates_Route_Prefix_Editor
 *
 * @author: Nerian
 * @last_edit:    24.08.2015
 */
class KL_EditorPostTemplates_Route_Prefix_Editor implements XenForo_Route_Interface
{
    public function match($routePath, Zend_Controller_Request_Http $request, XenForo_Router $router)
    {
        $action = $router->resolveActionWithIntegerParam($routePath, $request, 'template_id');
        $routeMatch = $router->getRouteMatch('KL_EditorPostTemplates_ControllerPublic_Templates', $action, 'account', $routePath);
        return $routeMatch;
    }

    public function buildLink($originalPrefix, $outputPrefix, $action, $extension, $data, array &$extraParams)
    {
        return XenForo_Link::buildBasicLinkWithIntegerParam($outputPrefix, $action, $extension, $data, 'template_id', 'title');
    }
}