<?php

/**
 * KL_EditorPostTemplates_Route_Prefix_InlineMod
 *
 * @author: Nerian
 * @last_edit:    27.08.2015
 */
class KL_EditorPostTemplates_Route_Prefix_InlineMod implements XenForo_Route_Interface
{
    /**
     * Match a specific route for an already matched prefix.
     *
     * @see XenForo_Route_Interface::match()
     */
    public function match($routePath, Zend_Controller_Request_Http $request, XenForo_Router $router)
    {
        $parts = explode('/', $routePath, 2);

        $controllerPart = str_replace(array('-', '/'), ' ', strtolower($parts[0]));
        $controllerPart = str_replace(' ', '', ucwords($controllerPart));

        $action = (isset($parts[0]) ? $parts[0] : '');

        return $router->getRouteMatch('KL_EditorPostTemplates_ControllerPublic_InlineMod_Templates', $action, 'account', $routePath);
    }
}