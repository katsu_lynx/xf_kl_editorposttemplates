<?php

/**
 * KL_EditorPostTemplates_Helpers
 *
 * @author: Nerian
 * @last_edit:    10.09.2015
 */
class KL_EditorPostTemplates_Helpers
{
    public static function helperTemplates()
    {
        $visitor = XenForo_Visitor::getInstance();
        $templateModel = new KL_EditorPostTemplates_Model_Editor;
        $adminTemplates = $templateModel->getAdminTemplatesForUser($visitor->user_id);
        $userTemplates = $templateModel->getUserTemplatesByUserId($visitor->user_id);

        $templateString = '';

        if($visitor->hasPermission('kl_ept_group', 'kl_ept_canUseTemplates')) {
            foreach ($adminTemplates as $category) {
                if (!empty($category['templates'])) {
                    $templateSubString = '';
                    foreach ($category['templates'] as $template) {
                        if (!$template['ignored']) {
                            $templateSubString .= '<option value="a' . $template['template_id'] . '">&nbsp;&nbsp;' . $template['title'] . '</option>';
                        }
                    }
                    if (strlen($templateSubString)) {
                        $templateString .= '<option disabled="disabled">' . $category['category_name'] . '</option>' . $templateSubString;
                    }
                }
            }
        }

        if ($visitor->hasPermission('kl_ept_group', 'kl_ept_createOwnTemplates')) {
            $userTemplateString = new XenForo_Phrase('kl_ept_user_templates');
            $templateString .= '<option disabled="disabled">' . $userTemplateString . '</option>';

            $emptyPhrase = new XenForo_Phrase('kl_ept_no_templates_yet');
            if(empty($userTemplates)) {
               $templateString .= '<option disabled="disabled">&nbsp;&nbsp;'.$emptyPhrase.'</option>';
            }
            foreach ($userTemplates as $template) {
                if ($template['active']) {
                    $templateString .= '<option value="u' . $template['template_id'] . '">&nbsp;&nbsp;' . $template['title'] . '</option>';
                }
            }
        }

        return $templateString;
    }

    public static function helperPostTemplatesScript()
    {
        $visitor = XenForo_Visitor::getInstance();
        $templateModel = new KL_EditorPostTemplates_Model_Editor;
        $adminTemplates = $templateModel->getAdminTemplatesForUser($visitor->user_id);
        $userTemplates = $templateModel->getUserTemplatesByUserId($visitor->user_id);

        $templates = array();
        if($visitor->hasPermission('kl_ept_group', 'kl_ept_canUseTemplates')) {
            foreach ($adminTemplates as $category) {
                foreach ($category['templates'] as $template) {
                    $templates['a' . $template['template_id']] = $template['content'];
                }
            }
        }
        if ($visitor->hasPermission('kl_ept_group', 'kl_ept_createOwnTemplates')) {
            foreach ($userTemplates as $template) {
                $templates['u' . $template['template_id']] = $template['content'];
            }
        }

        return json_encode($templates);
    }

    public static function helperPostTemplatesShow() {
        return count(json_decode(self::helperPostTemplatesScript()));
    }
}