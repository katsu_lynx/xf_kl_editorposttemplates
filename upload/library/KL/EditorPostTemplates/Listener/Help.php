<?php

/**
 * KL_EditorPostTemplates_Listener_Help
 *
 * @author: Nerian
 * @last_edit:    30.08.2015
 */
class KL_EditorPostTemplates_Listener_Help
{
    public static function extend($class, array &$extend)
    {
        $extend[] = 'KL_EditorPostTemplates_ControllerPublic_Help';
    }
}