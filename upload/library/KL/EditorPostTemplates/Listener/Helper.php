<?php

/**
 * KL_EditorPostTemplates_Listener_Helper
 *
 * @author: Nerian
 * @last_edit:    10.09.2015
 */
class KL_EditorPostTemplates_Listener_Helper
{
    public static function extend(XenForo_Dependencies_Abstract $dependencies, array $data)
    {
        //Get the static variable $helperCallbacks and add a new item in the array.
        XenForo_Template_Helper_Core::$helperCallbacks += array(
            'posttemplates' => array('KL_EditorPostTemplates_Helpers', 'helperTemplates'),
            'posttemplatesscript' => array('KL_EditorPostTemplates_Helpers', 'helperPostTemplatesScript'),
            'posttemplatesshow' => array('KL_EditorPostTemplates_Helpers', 'helperPostTemplatesShow')
        );
    }
}