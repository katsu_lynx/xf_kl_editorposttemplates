<?php

/**
 * KL_EditorPostTemplates_Model_Editor
 *
 * @author: Nerian
 * @last_edit:    30.08.2015
 */
class KL_EditorPostTemplates_Model_Editor extends XenForo_Model
{

    /*
     * TYPE: USER
     * @return array
     * @last_edit: 24.08.2015
     */
    public function getUserTemplates()
    {
        return $this->_getDb()->fetchAll('
			SELECT *
            FROM xf_kl_ept_post_template_user
        ');
    }

    /*
     * TYPE: USER
     * @return array
     * @last_edit: 24.08.2015
     */
    public function getUserTemplatesByUserId($id)
    {
        return $this->_getDb()->fetchAll('
			SELECT *
            FROM xf_kl_ept_post_template_user
            WHERE user_id = ?
		', $id);
    }

    /*
     * TYPE: USER
     * @return array
     * @last_edit: 24.08.2015
     */
    public function getUserTemplateById($id)
    {
        return $this->_getDb()->fetchRow('
			SELECT *
            FROM xf_kl_ept_post_template_user
            WHERE template_id = ?
		', $id);
    }

    /*
     * TYPE: USER
     * @return boolean
     * @last_edit: 24.08.2015
     */
    public function isTemplateFromUser($template)
    {
        $dbTemplate = $this->_getDb()->fetchRow('
            SELECT *
            FROM xf_kl_ept_post_template_user
            WHERE template_id = ?
        ', $template['template_id']);

        return $dbTemplate['user_id'] == $template['user_id'];
    }

    /*
     * TYPE: USER
     * @return array
     * @last_edit: 30.08.2015
     */
    public function getAdminTemplatesForUser($uid)
    {
        $templates = $this->_getDb()->fetchAll('
        SELECT a.*, (i.template_id is not null) as ignored
        FROM xf_kl_ept_post_template_admin a
        LEFT JOIN (
            SELECT DISTINCT *
            FROM xf_kl_ept_post_template_user_ignore i
            WHERE i.user_id = ?
        ) AS i ON a.template_id = i.template_id
        WHERE a.active = 1
        ', $uid);

        $tcategories = $this->_getDb()->fetchAll('
			SELECT *
            FROM xf_kl_ept_post_template_admin_categories
            ORDER BY position, category_id
		');

        foreach ($tcategories as $category) {
            $category['templates'] = array();
            $categories[$category['category_id']] = $category;
        }

        foreach ($templates as $template) {
            $categories[$template['category_id']]['templates'][] = $template;
        }

        return isset($categories) ? $categories : array();
    }

    /*
     * TYPE: USER
     * @return array
     * @last_edit: 30.08.2015
     */
    public function getIgnoreData($id)
    {
        $return = $this->_getDb()->fetchRow('
            SELECT *
            FROM xf_kl_ept_post_template_user_ignore
            WHERE id = ?
        ', $id);

        return $return;
    }

    /*
     * TYPE: USER
     * @return array
     * @last_edit: 30.08.2015
     */
    public function getIgnoredTemplatesByUserId($uid)
    {
        return $this->_getDb()->fetchAll('
            SELECT *
            FROM xf_kl_ept_post_template_user_ignore
            WHERE user_id = ?
        ', $uid);
    }

    /*
     * TYPE: ADMIN
     * @return array
     * @last_edit: 23.07.2015
     */
    public function getAdminTemplates()
    {
        $templates = $this->_getDb()->fetchAll('
			SELECT *
            FROM xf_kl_ept_post_template_admin
            ORDER BY position, template_id
		');

        $tcategories = $this->_getDb()->fetchAll('
			SELECT *
            FROM xf_kl_ept_post_template_admin_categories
            ORDER BY position, category_id
		');

        foreach ($tcategories as $category) {
            $category['templates'] = array();
            $categories[$category['category_id']] = $category;
        }

        foreach ($templates as $template) {
            $categories[$template['category_id']]['templates'][] = $template;
        }

        return isset($categories) ? $categories : array();
    }

    /*
     * TYPE: ADMIN
     * @return array
     * @last_edit: 24.07.2015
     */
    public function getAdminTemplatesPlain()
    {
        return $this->_getDb()->fetchAll('
			SELECT *
            FROM xf_kl_ept_post_template_admin
		');
    }

    /*
     * TYPE: ADMIN
     * @return array
     * @last_edit: 23.07.2015
     */
    public function getAdminTemplateById($id)
    {
        return $this->_getDb()->fetchRow('
			SELECT *
            FROM xf_kl_ept_post_template_admin
            WHERE template_id = ?
		', $id);
    }

    /*
     * TYPE: ADMIN
     * @return array
     * @last_edit: 23.07.2015
     */
    public function getCategories()
    {
        return $this->_getDb()->fetchAll('
            SELECT *
            FROM xf_kl_ept_post_template_admin_categories
            ORDER BY position
        ');
    }

    /*
     * TYPE: ADMIN
     * @return array
     * @last_edit: 23.07.2015
     */
    public function getCategoryById($id)
    {
        return $this->_getDb()->fetchRow('
            SELECT *
            FROM xf_kl_ept_post_template_admin_categories
            WHERE category_id = ?
        ', $id);
    }
}