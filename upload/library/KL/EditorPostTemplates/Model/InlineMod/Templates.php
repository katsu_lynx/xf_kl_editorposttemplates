<?php

/**
 * KL_EditorPostTemplates_Model_InlineMod_Templates
 *
 * @author: Nerian
 * @last_edit:    27.08.2015
 */
class KL_EditorPostTemplates_Model_InlineMod_Templates extends XenForo_Model
{
    public function getFilteredTemplatesByIds($templates)
    {
        $visitor = XenForo_Visitor::getInstance();
        +
        $templates = implode(',', $templates);

        return $this->_getDb()->fetchAll('
            SELECT *
            FROM xf_kl_ept_post_template_user
            WHERE user_id = ? AND template_id in(?)
            ', array($visitor->user_id, $templates)
        );
    }
}