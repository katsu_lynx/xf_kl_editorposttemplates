<?php

/**
 * KL_EditorPostTemplates_ControllerAdmin_Editor
 *
 * @author: Nerian
 * @last_edit:    23.08.2015
 */
class KL_EditorPostTemplates_ControllerAdmin_Editor extends XenForo_ControllerAdmin_Abstract
{

    /* 
     * TYPE: OVERVIEW
     * PURPOSE: RETURN LIST OF ADMIN TEMPLATES
     * @last_edit: 23.08.2015
     * @params: templates
     * @return: responseView
     */
    public function actionIndex()
    {
        /* Gather required variables */
        /*
        $nodes = $this->_getNodeModel()->getAllNodes();
        $categories = $this->_getCategoryModel()->getAllCategories();
        */
        $templates = $this->_getTemplateModel()->getAdminTemplates();

        /* Define string for each template showing the Forums and Resource Manager Categories, it is allowed in */

        foreach ($templates as &$templateCategory) {
            if (!isset($templateCategory['templates']))
                $templateCategory['templates'] = array();

            foreach ($templateCategory['templates'] as &$template) {
                /*
                $template['forums'] = array_column(unserialize($template['forums']), 1);
                $template['resource_manager_categories'] = array_column(unserialize($template['resource_manager_categories']), 1);

                $is_all = array('forums' => 0, 'rmcs' => 0);
                $columns = array();

                if (!in_array('all', $template['forums'])) {
                    if (!in_array('all', $template['resource_manager_categories']))
                        $columns = array_merge($template['forums'], $template['resource_manager_categories']);
                    else {
                        $columns = $template['forums'];
                        $is_all['rmcs'] = 1;
                    }
                } else {
                    $is_all['forums'] = 1;
                    if (!in_array('all', $template['resource_manager_categories']))
                        $columns = $template['resource_manager_categories'];
                    else
                        $is_all['rmcs'] = 1;
                }

                $template['is_all'] = $is_all;
                $template['string'] = implode(', ', $columns);
                */
                $template['string'] = '';
            }
        }

        $params = array(
            'templates' => $templates
        );

        return $this->responseView('KL_EditorPostTemplates_ViewAdmin_Editor', 'kl_ept_list', $params);
    }

    /*
     * TYPE: TEMPLATE
     * ACTION: TOGGLE
     * PURPOSE: TOGGLE ADMIN TEMPLATE ACTIVE STATE
     * @last_edit: 24.07.2015
     * @return: responseRedirect
     */
    public function actionToggle()
    {
        $this->_assertPostOnly();

        /* Gather required variables */
        $items = $this->_getTemplateModel()->getAdminTemplatesPlain();
        $idExists = $this->_input->filterSingle('exists', array(XenForo_Input::UINT, 'array' => true));
        $ids = $this->_input->filterSingle('id', array(XenForo_Input::UINT, 'array' => true));

        /* Save changes */
        foreach ($items AS $item) {
            $inputId = $item['template_id'];

            if (isset($idExists[$inputId])) {
                $itemActive = (!empty($ids[$inputId]) ? 1 : 0);

                if ($item['active'] != $itemActive) {
                    $dw = XenForo_DataWriter::create('KL_EditorPostTemplates_DataWriter_AdminTemplate');
                    $dw->setExistingData($item, true);
                    $dw->set('active', $itemActive);
                    $dw->save();
                }
            }
        }

        return $this->responseRedirect(
            XenForo_ControllerResponse_Redirect::SUCCESS,
            XenForo_Link::buildAdminLink('add-ons')
        );
    }

    /*
     * TYPE: TEMPLATE
     * ACTION: ADD/EDIT
     * PURPOSE: ADD/EDIT ADMIN TEMPLATE
     * @last_edit: 25.07.2015
     * @params: template, nodes, categories, resource_categories
     * @return: responseView
     */
    public function actionEdit()
    {
        /* Gather required variables */
        $templateId = $this->_input->filterSingle('template_id', XenForo_Input::UINT);
        $templateModel = $this->_getTemplateModel();
        $addons = XenForo_Application::get('addOns');
        $template = $templateId ? $templateModel->getAdminTemplateById($templateId) : array();

        /* Gather template and unserialize blobs */
        /*
        $template['forums'] = !empty($template['forums']) ? array_column(unserialize($template['forums']), 0) : array();
        $template['resource_manager_categories'] = !empty($template['resource_manager_categories']) ? array_column(unserialize($template['resource_manager_categories']), 0) : array();
        */

        $template['forums'] = array();
        $template['resource_manager_categories'] = array();

        $is_all = array(
            'forums' => in_array(0, $template['forums']),
            'rmcs' => in_array(0, $template['resource_manager_categories'])
        );
        $is_rm = isset($addons['XenResource']);

        /* Define viewParams */
        $params = array(
            'template' => $template,
            'nodes' => $this->_getNodeModel()->getAllNodes(),
            'categories' => $templateModel->getCategories(),
            'is_all' => $is_all,
            'resource_categories' => $is_rm ? $this->_getCategoryModel()->getAllCategories() : array(),
            'is_resource_manager' => $is_rm
        );

        return $this->responseView('KL_EditorPostTemplates_ViewAdmin_Editor_Edit', 'kl_ept_template_edit', $params);
    }

    /*
     * TYPE: TEMPLATE
     * ACTION: SAVE
     * PURPOSE: SAVE ADMIN TEMPLATE
     * @last_edit: 25.07.2015
     * @return: responseRedirect
     */
    public function actionSave()
    {
        /* Gather required variables */
        $templateId = $this->_input->filterSingle('template_id', XenForo_Input::UINT);
        $template = $this->_input->filter(array(
            'title' => XenForo_Input::STRING,
            'category_id' => XenForo_Input::UINT,
            'active' => XenForo_Input::UINT,
            'position' => XenForo_Input::UINT
        ));

        $template['forums'] = $this->_input->filterSingle('forums', XenForo_Input::ARRAY_SIMPLE);
        $template['resource_manager_categories'] = $this->_input->filterSingle('resource_manager_categories', XenForo_Input::ARRAY_SIMPLE);

        $templateContent = $this->getHelper('Editor')->getMessageText('content', $this->_input);
        $template['content'] = XenForo_Helper_String::autoLinkBbCode($templateContent);

        foreach ($template['forums'] as &$forum)
            $forum = explode('.', $forum, 2);
        foreach ($template['resource_manager_categories'] as &$category)
            $category = explode('.', $category, 2);

        $template['forums'] = serialize($template['forums']);
        $template['resource_manager_categories'] = serialize($template['resource_manager_categories']);

        /* Save changes */
        $dw = XenForo_DataWriter::create('KL_EditorPostTemplates_DataWriter_AdminTemplate');
        if (isset($templateId) && $templateId)
            $dw->setExistingData(array('template_id' => $templateId));
        $dw->bulkSet($template);
        $dw->save();

        return $this->responseRedirect(
            XenForo_ControllerResponse_Redirect::SUCCESS,
            XenForo_Link::buildAdminLink('kl-ept')
        );
    }

    /*
     * TYPE: TEMPLATE
     * ACTION: DELETE
     * PURPOSE: DELETE ADMIN TEMPLATE
     * @last_edit: 23.07.2015
     * @params: template
     * @return: responseRedirect/responseView
     */
    public function actionDelete()
    {
        /* Gather required variables */
        $templateId = $this->_input->filterSingle('template_id', XenForo_Input::UINT);
        $templateModel = $this->_getTemplateModel();

        /* Delete content */
        if ($this->isConfirmedPost()) {
            $dw = XenForo_DataWriter::create('KL_EditorPostTemplates_DataWriter_AdminTemplate');
            $dw->setExistingData(array('template_id' => $templateId));
            $dw->delete();

            return $this->responseRedirect(
                XenForo_ControllerResponse_Redirect::SUCCESS,
                XenForo_Link::buildAdminLink('kl-ept')
            );
        } /* Send confirmation question */
        else {
            $template = $templateModel->getAdminTemplateById($templateId);
            $params = array(
                'template' => $template
            );

            return $this->responseView('KL_EditorPostTemplates_ViewAdmin_Template_Delete', 'kl_ept_template_delete', $params);
        }
    }

    /*
     * TYPE: CATEGORY
     * ACTION: ADD/EDIT
     * PURPOSE: ADD/EDIT ADMIN TEMPLATE CATEGORY
     * @last_edit: 23.07.2015
     * @params: category
     * @return: responseView
     */
    public function actionCategoryEdit()
    {
        /* Gather required variables */
        $templateModel = $this->_getTemplateModel();
        $categoryId = $this->_input->filterSingle('template_id', XenForo_Input::UINT);

        /* Define viewParams */
        $params = array(
            'category' => ($categoryId ? $templateModel->getCategoryById($categoryId) : array())
        );

        return $this->responseView('KL_EditorPostTemplates_ViewAdmin_Editor_Edit', 'kl_ept_category_edit', $params);
    }

    /*
     * TYPE: CATEGORY
     * ACTION: SAVE
     * PURPOSE: SAVE ADMIN TEMPLATE CATEGORY
     * @last_edit: 23.07.2015
     * @return: responseRedirect
     */
    public function actionCategorySave()
    {
        /* Gather required variables */
        $categoryId = $this->_input->filterSingle('category_id', XenForo_Input::UINT);
        $category = $this->_input->filter(array(
            'category_name' => XenForo_Input::STRING,
            'position' => XenForo_Input::UINT
        ));

        /* Save changes */
        $dw = XenForo_DataWriter::create('KL_EditorPostTemplates_DataWriter_Category');
        if (isset($categoryId) && $categoryId)
            $dw->setExistingData(array('category_id' => $categoryId));
        $dw->bulkSet($category);
        $dw->save();

        return $this->responseRedirect(
            XenForo_ControllerResponse_Redirect::SUCCESS,
            XenForo_Link::buildAdminLink('kl-ept')
        );
    }

    /*
     * TYPE: CATEGORY
     * ACTION: DELETE
     * PURPOSE: DELETE ADMIN TEMPLATE CATEGORY
     * @last_edit: 23.07.2015
     * @params: category
     * @return: responseView/responseRedirect
     */
    public function actionCategoryDelete()
    {
        /* Gather required variables */
        $categoryId = $this->_input->filterSingle('template_id', XenForo_Input::UINT);
        $templateModel = $this->_getTemplateModel();

        /* Delete content */
        if ($this->isConfirmedPost()) {
            $dw = XenForo_DataWriter::create('KL_EditorPostTemplates_DataWriter_Category');
            $dw->setExistingData(array('category_id' => $categoryId));
            $dw->delete();

            return $this->responseRedirect(
                XenForo_ControllerResponse_Redirect::SUCCESS,
                XenForo_Link::buildAdminLink('kl-ept')
            );
        } /* Send confirmation question */
        else {
            $category = $templateModel->getCategoryById($categoryId);
            $params = array(
                'category' => $category
            );

            return $this->responseView('KL_EditorPostTemplates_ViewAdmin_Category_Delete', 'kl_ept_category_delete', $params);
        }
    }

    /* 
     * TYPE: HELPER 
     * @return XenForo_Model_Node
     */
    protected function _getNodeModel()
    {
        return $this->getModelFromCache('XenForo_Model_Node');
    }

    /* 
     * TYPE: HELPER 
     * @return XenResource_Model_Category
     */
    protected function _getCategoryModel()
    {
        return $this->getModelFromCache('XenResource_Model_Category');
    }

    /* 
     * TYPE: HELPER 
     * @return KL_EditorPostTemplates_Model_Editor
     */
    protected function _getTemplateModel()
    {
        return $this->getModelFromCache('KL_EditorPostTemplates_Model_Editor');
    }

}