<?php

/**
 * KL_EditorPostTemplates_DataWriter_AdminTemplate
 *
 * @author: Nerian
 * @last_edit:    24.07.2015
 */
class KL_EditorPostTemplates_DataWriter_AdminTemplate extends XenForo_DataWriter
{
    protected function _getFields()
    {
        return array(
            'xf_kl_ept_post_template_admin' => array(
                'template_id' => array('type' => self::TYPE_UINT, 'autoIncrement' => true),
                'title' => array('type' => self::TYPE_STRING, 'required' => true),
                'category_id' => array('type' => self::TYPE_UINT, 'required' => true),
                'forums' => array('type' => self::TYPE_SERIALIZED, 'default' => 'a:0:{}'),
                'resource_manager_categories' => array('type' => self::TYPE_SERIALIZED, 'default' => 'a:0:{}'),
                'template_date' => array('type' => self::TYPE_UINT, 'default' => XenForo_Application::$time),
                'content' => array('type' => self::TYPE_STRING, 'default' => ''),
                'active' => array('type' => self::TYPE_UINT, 'default' => 1),
                'position' => array('type' => self::TYPE_UINT, 'default' => 10)
            )
        );
    }

    /*
     * @return array
     */
    protected function _getExistingData($data)
    {
        if (!is_array($data) || !isset($data['template_id']))
            return false;

        return array('xf_kl_ept_post_template_admin' => $this->_getTemplateModel()->getAdminTemplateById($data['template_id']));
    }

    /*
     * @return string
     */
    protected function _getUpdateCondition($tableName)
    {
        return 'template_id = ' . $this->_db->quote($this->getExisting('template_id'));
    }

    /* 
     * TYPE: HELPER 
     * @return KL_EditorPostTemplates_Model_Editor
     */
    protected function _getTemplateModel()
    {
        return $this->getModelFromCache('KL_EditorPostTemplates_Model_Editor');
    }

}