<?php

/**
 * KL_EditorPostTemplates_DataWriter_UserTemplate
 *
 * @author: Nerian
 * @last_edit:    27.08.2015
 */
class KL_EditorPostTemplates_DataWriter_UserTemplate extends XenForo_DataWriter
{
    protected function _getFields()
    {
        return array(
            'xf_kl_ept_post_template_user' => array(
                'template_id' => array('type' => self::TYPE_UINT, 'autoIncrement' => true),
                'title' => array('type' => self::TYPE_STRING, 'required' => true),
                'user_id' => array('type' => self::TYPE_UINT, 'required' => true),
                'template_date' => array('type' => self::TYPE_UINT, 'default' => XenForo_Application::$time),
                'template_edit_date' => array('type' => self::TYPE_UINT, 'default' => XenForo_Application::$time),
                'content' => array('type' => self::TYPE_STRING, 'default' => ''),
                'active' => array('type' => self::TYPE_UINT, 'default' => 1)
            )
        );
    }

    /*
     * @return array
     */
    protected function _getExistingData($data)
    {
        if (!is_array($data) || !isset($data['template_id']))
            return false;

        return array('xf_kl_ept_post_template_user' => $this->_getTemplateModel()->getUserTemplateById($data['template_id']));
    }

    /*
     * @return string
     */
    protected function _getUpdateCondition($tableName)
    {
        return 'template_id = ' . $this->_db->quote($this->getExisting('template_id'));
    }

    /* 
     * TYPE: HELPER 
     * @return KL_EditorPostTemplates_Model_Editor
     */
    protected function _getTemplateModel()
    {
        return $this->getModelFromCache('KL_EditorPostTemplates_Model_Editor');
    }
}