<?php

/**
 * KL_EditorPostTemplates_DataWriter_Ignore
 *
 * @author: Nerian
 * @last_edit:    30.08.2015
 */
class KL_EditorPostTemplates_DataWriter_Ignore extends XenForo_DataWriter
{
    protected function _getFields()
    {
        return array(
            'xf_kl_ept_post_template_user_ignore' => array(
                'id' => array('type' => self::TYPE_UINT, 'autoIncrement' => true),
                'template_id' => array('type' => self::TYPE_UINT, 'required' => true),
                'user_id' => array('type' => self::TYPE_UINT, 'required' => true)
            )
        );
    }

    /*
     * @return array
     */
    protected function _getExistingData($data)
    {
        if (!is_array($data) || !isset($data['id']))
            return false;

        return array('xf_kl_ept_post_template_user_ignore' => $this->_getTemplateModel()->getIgnoreData($data['id']));
    }

    /*
     * @return string
     */
    protected function _getUpdateCondition($tableName)
    {
        return 'id = ' . $this->_db->quote($this->getExisting('id'));
    }

    /* 
     * TYPE: HELPER 
     * @return KL_EditorPostTemplates_Model_Editor
     */
    protected function _getTemplateModel()
    {
        return $this->getModelFromCache('KL_EditorPostTemplates_Model_Editor');
    }
}