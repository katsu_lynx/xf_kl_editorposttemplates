<?php

/**
 * KL_EditorPostTemplates_DataWriter_Category
 *
 * @author: Nerian
 * @last_edit:    24.07.2015
 */
class KL_EditorPostTemplates_DataWriter_Category extends XenForo_DataWriter
{
    protected function _getFields()
    {
        return array(
            'xf_kl_ept_post_template_admin_categories' => array(
                'category_id' => array('type' => self::TYPE_UINT, 'auto_increment' => true),
                'category_name' => array('type' => self::TYPE_STRING, 'required' => true),
                'position' => array('type' => self::TYPE_UINT, 'required' => true, 'default' => 10)
            )
        );
    }

    protected function _getExistingData($data)
    {
        if (!is_array($data) || !isset($data['category_id']))
            return false;

        return array('xf_kl_ept_post_template_admin_categories' => $this->_getTemplateModel()->getCategoryById($data['category_id']));
    }

    protected function _getUpdateCondition($tableName)
    {
        return 'category_id = ' . $this->_db->quote($this->getExisting('category_id'));
    }

    protected function _getTemplateModel()
    {
        return $this->getModelFromCache('KL_EditorPostTemplates_Model_Editor');
    }
}