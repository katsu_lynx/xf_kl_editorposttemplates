<?php

/**
 * KL_EditorPostTemplates_ControllerPublic_InlineMod_Templates
 *
 * @author: Nerian
 * @last_edit:    27.08.2015
 */
class KL_EditorPostTemplates_ControllerPublic_InlineMod_Templates extends XenForo_ControllerPublic_InlineMod_Abstract
{
    public $inlineModKey = 'templates';

    /* 
     * TYPE: DELETE
     * @last_edit: 27.08.2015
     * @params: templates
     * @return: responseRedirect, responseView
     */
    public function actionDelete()
    {
        $templateModel = $this->getInlineModTypeModel();
        $templates = $this->_input->filterSingle('templates', XenForo_Input::ARRAY_SIMPLE);
        $templates = $templateModel->getFilteredTemplatesByIds($templates);

        if ($this->isConfirmedPost()) {
            $dw = XenForo_DataWriter::create('KL_EditorPostTemplates_DataWriter_UserTemplate');

            $this->clearCookie();

            foreach ($templates as $template) {
                $dw->setExistingData($template);
                $dw->delete();
                $dw->save();
            }

            return $this->responseRedirect(
                XenForo_ControllerResponse_Redirect::SUCCESS,
                $this->getDynamicRedirect()
            );
        } else {
            $redirect = $this->getDynamicRedirect();

            $viewParams = array(
                'templates' => $templates,
                'templateCount' => count($templates),
                'redirect' => $redirect,
            );

            return $this->responseView('KL_EditorPostTemplates_ViewPublic_InlineMod_Delete', 'kl_ept_inline_mod_delete', $viewParams);
        }
    }

    /* 
     * TYPE: Activate/Deactivate
     * @last_edit: 27.08.2015
     * @params: templates
     * @return: responseRedirect
     */
    public function actionActivate()
    {
        $templateModel = $this->getInlineModTypeModel();
        $templates = $this->_input->filterSingle('templates', XenForo_Input::ARRAY_SIMPLE);
        $templates = $templateModel->getFilteredTemplatesByIds($templates);
        $dw = XenForo_DataWriter::create('KL_EditorPostTemplates_DataWriter_UserTemplate');

        foreach ($templates as $template) {
            $dw->setExistingData($template);
            $dw->set('active', 1);
            $dw->save();
        }

        $this->clearCookie();

        return $this->responseRedirect(
            XenForo_ControllerResponse_Redirect::SUCCESS,
            $this->getDynamicRedirect()
        );
    }

    /* 
     * TYPE: Activate/Deactivate
     * @last_edit: 27.08.2015
     * @params: templates
     * @return: responseRedirect
     * TODO: COPY OF actionActivate, REWORK!
     */
    public function actionDeactivate()
    {
        $templateModel = $this->getInlineModTypeModel();
        $templates = $this->_input->filterSingle('templates', XenForo_Input::ARRAY_SIMPLE);
        $templates = $templateModel->getFilteredTemplatesByIds($templates);
        $dw = XenForo_DataWriter::create('KL_EditorPostTemplates_DataWriter_UserTemplate');

        foreach ($templates as $template) {
            $dw->setExistingData($template);
            $dw->set('active', 0);
            $dw->save();
        }

        $this->clearCookie();

        return $this->responseRedirect(
            XenForo_ControllerResponse_Redirect::SUCCESS,
            $this->getDynamicRedirect()
        );
    }

    /**
     * @return KL_EditorPostTemplates_Model_InlineMod_Templates
     */
    public function getInlineModTypeModel()
    {
        return $this->getModelFromCache('KL_EditorPostTemplates_Model_InlineMod_Templates');
    }
}