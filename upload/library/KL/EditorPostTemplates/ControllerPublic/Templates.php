<?php

/**
 * KL_EditorPostTemplates_ControllerPublic_Templates
 *
 * @author: Nerian
 * @last_edit:    24.08.2015
 */
class KL_EditorPostTemplates_ControllerPublic_Templates extends XenForo_ControllerPublic_Abstract
{

    /* 
     * TYPE: OVERVIEW
     * PURPOSE: RETURN LIST OF USER TEMPLATES
     * @last_edit: 24.08.2015
     * @params: templates
     * @return: responseView
     */
    public function actionIndex()
    {
        /* gather required stuff */
        $templateModel = $this->_getTemplateModel();
        $visitor = XenForo_Visitor::getInstance();
        $userTemplates = $templateModel->getUserTemplatesByUserId($visitor->user_id);

        /* check for permission */
        if (!$visitor->hasPermission('kl_ept_group', 'kl_ept_createOwnTemplates')) {
            return $this->responseNoPermission();
        }

        /* Prepare viewParams */
        $viewParams = array(
            'templates' => $userTemplates,
            'numOfTemplates' => count($userTemplates),
            'maxTemplates' => $visitor->hasPermission('kl_ept_group', 'kl_ept_numberOfTemplates'),
            'canAddTemplates' => $visitor->hasPermission('kl_ept_group', 'kl_ept_createOwnTemplates')
                && (count($userTemplates) < $visitor->hasPermission('kl_ept_group', 'kl_ept_numberOfTemplates')
                    || $visitor->hasPermission('kl_ept_group', 'kl_ept_numberOfTemplates') < 1)
        );

        return $this->responseView(
            'KL_EditorPostTemplates_ViewPublic_Templates',
            'kl_ept_template_list',
            $viewParams
        );
    }

    /* 
     * TYPE: ADD/EDIT
     * @last_edit: 24.08.2015
     * @params: template
     * @return: responseView
     */
    public function actionAdd()
    {
        return $this->actionEdit();
    }

    /* 
     * TYPE: ADD/EDIT
     * @last_edit: 24.08.2015
     * @params: template
     * @return: responseView
     */
    public function actionAddInline()
    {
        return $this->actionEditInline();
    }

    /* 
     * TYPE: ADD/EDIT
     * @last_edit: 24.08.2015
     * @params: templates
     * @return: responseView
     */
    public function actionEdit()
    {
        /* gather required stuff */
        $userTemplateId = $this->_input->filterSingle('template_id', XenForo_Input::UINT);
        $templateModel = $this->_getTemplateModel();
        $userTemplate = $templateModel->getUserTemplateById($userTemplateId);
        $visitor = XenForo_Visitor::getInstance();
        $numOfTemplates = count($templateModel->getUserTemplatesByUserId($visitor->user_id));

        /* check for permission */
        if (!$visitor->hasPermission('kl_ept_group', 'kl_ept_createOwnTemplates')
            || (($visitor->hasPermission('kl_ept_group', 'kl_ept_numberOfTemplates') <= $numOfTemplates)
                && $visitor->hasPermission('kl_ept_group', 'kl_ept_numberOfTemplates') > 0)
        ) {
            return $this->responseNoPermission();
        }

        /* save as new template if template id does not relate to user */
        if (!$templateModel->isTemplateFromUser($userTemplate)) {
            unset($userTemplate);
        }

        if (!isset($userTemplate)) {
            $userTemplate = array('title' => '', 'content' => '', 'is_active' => 1);
        }

        /* Prepare viewParams */
        $viewParams = array(
            'template' => $userTemplate
        );

        return $this->responseView(
            'KL_EditorPostTemplates_ViewPublic_Templates_Edit',
            'kl_ept_template_edit',
            $viewParams
        );
    }

    /* 
     * TYPE: ADD/EDIT
     * @last_edit: 24.08.2015
     * @params: template
     * @return: responseView
     */
    public function actionEditInline()
    {
        $return = $this->actionEdit();
        $return->templateName = 'kl_ept_template_edit_inline';
        return $return;
    }

    /* 
     * TYPE: ADD/EDIT
     * @last_edit: 24.08.2015
     * @params: templates
     * @return: responseRedirect
     */
    public function actionUpdate()
    {
        $this->_assertPostOnly();

        /* gather required stuff */
        $dw = XenForo_DataWriter::create('KL_EditorPostTemplates_DataWriter_UserTemplate');
        $templateModel = $this->_getTemplateModel();
        $visitor = XenForo_Visitor::getInstance();
        $numOfTemplates = count($templateModel->getUserTemplatesByUserId($visitor->user_id));
        $template = $this->_input->filter(array(
            'title' => XenForo_Input::STRING,
            'template_id' => XenForo_Input::UINT,
            'active' => XenForo_Input::UINT
        ));

        $template['user_id'] = $visitor->user_id;
        $template['content'] = $this->getHelper('Editor')->getMessageText('content', $this->_input);
        $template['content'] = XenForo_Helper_String::autoLinkBbCode($template['content']);
        $template['template_edit_date'] = XenForo_Application::$time;


        /* check for permission */
        if (!$visitor->hasPermission('kl_ept_group', 'kl_ept_createOwnTemplates')
            || (($visitor->hasPermission('kl_ept_group', 'kl_ept_numberOfTemplates') <= $numOfTemplates)
                && $visitor->hasPermission('kl_ept_group', 'kl_ept_numberOfTemplates') > 0)
        ) {
            return $this->responseNoPermission();
        }
        
        /* save as new template if template id does not relate to user */
        if (!$templateModel->isTemplateFromUser($template)) {
            unset($template['template_id']);
        }

        /* Save Template */
        if (isset($template['template_id'])) {
            $dw->setExistingData($template);
        }


        $dw->bulkSet($template);
        $dw->save();
        return $this->responseRedirect(
            XenForo_ControllerResponse_Redirect::SUCCESS,
            XenForo_Link::buildPublicLink('post-templates')
        );
    }

    /* 
     * TYPE: HELPER 
     * @return KL_EditorPostTemplates_Model_Editor
     */
    protected function _getTemplateModel()
    {
        return $this->getModelFromCache('KL_EditorPostTemplates_Model_Editor');
    }


    /**
     * Session activity details.
     * @see XenForo_Controller::getSessionActivityDetailsForList()
     */
    public static function getSessionActivityDetailsForList(array $activities)
    {
        return new XenForo_Phrase('managing_account_details');
    }
}