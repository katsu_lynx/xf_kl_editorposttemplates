<?php

/**
 * KL_EditorPostTemplates_ControllerPublic_Help
 *
 * @author: Nerian
 * @last_edit:    30.08.2015
 */
class KL_EditorPostTemplates_ControllerPublic_Help extends XFCP_KL_EditorPostTemplates_ControllerPublic_Help
{
    public function actionTemplates()
    {
        /* gather required stuff */
        $templateModel = $this->_getTemplateModel();
        $visitor = XenForo_Visitor::getInstance();
        $templates = $templateModel->getAdminTemplatesForUser($visitor->user_id);
        $numOfTemplates = $visitor->hasPermission('kl_ept_group', 'kl_ept_numberOfTemplates');

        /* prepare viewParams */
        $viewParams = array(
            'templates' => $templates,
            'createOwnTemplates' => $visitor->hasPermission('kl_ept_group', 'kl_ept_createOwnTemplates'),
            'useAdminTemplates' => $visitor->hasPermission('kl_ept_group', 'kl_ept_canUseTemplates'),
            'ignoreTemplates' => $visitor->hasPermission('kl_ept_group', 'kl_ept_canIgnoreTemplates'),
            'maxTemplates' => $numOfTemplates > 0 ? $numOfTemplates : 0
        );

        return $this->_getWrapper('templates',
            $this->responseView('XenForo_ViewPublic_Help_BbCodes', 'kl_ept_help_templates', $viewParams)
        );
    }

    public function actionTemplatesSave()
    {
        /* gather required stuff */
        $templateModel = $this->_getTemplateModel();
        $visitor = XenForo_Visitor::getInstance();
        $templates = $this->_input->filterSingle('templates', XenForo_Input::ARRAY_SIMPLE);
        $aTemplates = $templateModel->getAdminTemplatesPlain();
        $userId = $visitor->user_id;
        $ignored = $templateModel->getIgnoredTemplatesByUserId($userId);
        $ignoredKeys = array();

        foreach ($ignored as $i) {
            $ignoredKeys[] = $i['template_id'];
        }

        foreach ($aTemplates as $template) {
            /* start ignoring template */
            if (!in_array($template['template_id'], $ignoredKeys) && !in_array($template['template_id'], $templates)) {
                $dw = XenForo_DataWriter::create('KL_EditorPostTemplates_DataWriter_Ignore');
                $dw->bulkSet(array(
                    'template_id' => $template['template_id'],
                    'user_id' => $userId
                ));
                $dw->save();
            } /* Ignore template no longer */
            else {
                foreach ($ignored as $searchTemplate) {
                    if ($searchTemplate['template_id'] == $template['template_id']) {
                        $dw = XenForo_DataWriter::create('KL_EditorPostTemplates_DataWriter_Ignore');
                        $dw->setExistingData($searchTemplate);
                        $dw->delete();
                    }
                }
            }
        }

        return $this->responseRedirect(
            XenForo_ControllerResponse_Redirect::SUCCESS,
            XenForo_Link::buildPublicLink('help/templates')
        );
    }

    /* 
     * TYPE: HELPER 
     * @return KL_EditorPostTemplates_Model_Editor
     */
    protected function _getTemplateModel()
    {
        return $this->getModelFromCache('KL_EditorPostTemplates_Model_Editor');
    }
}