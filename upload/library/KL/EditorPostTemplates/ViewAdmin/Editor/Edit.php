<?php

/**
 * KL_EditorPostTemplates_ViewAdmin_Editor_Edit
 *
 * @author: Nerian
 * @last_edit:    24.07.2015
 */
class KL_EditorPostTemplates_ViewAdmin_Editor_Edit extends XenForo_ViewAdmin_Base
{
    public function renderHtml()
    {
        XenForo_Application::set('view', $this);

        $this->_params['contentEditor'] = XenForo_ViewPublic_Helper_Editor::getEditorTemplate(
            $this, 'content', isset($this->_params['template']['content']) ? $this->_params['template']['content'] : ''
        );
    }
}