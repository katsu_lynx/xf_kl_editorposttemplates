/**
 * KL_EditorPostTemplates_extend
 *
 *	@author: Katsulynx
 *  @last_edit:	11.02.2016
 */
if(typeof RedactorPlugins == 'undefined') var RedactorPlugins = {};

!function($, window, document, undefined) {
    if(showtemplatebutton) {
        $(document).on('EditorInit', function(e, data){
            var editorFramework = data.editor,
                config = data.config;

            var eptButtonCallback = function(ed, ev) {
                ed.saveSelection();

                var modalTitle = editorFramework.getText('kl_ept_title');

                ed.modalInit(modalTitle, { url: editorFramework.dialogUrl + '&dialog=klEpt' }, 600, $.proxy(function() {
                    $('#kl_ept_template_chooser').append(templatesOption);
                    $('#kl_ept_template_chooser').on('change', function() {
                        var templateId = $('#kl_ept_template_chooser').val();
                        $('#kl_ept_template_content').val(
                            templates[templateId]
                        );
                    });

                    $('#kl_ept_template_insert').click(function(e) {

                        var output = $('#kl_ept_template_content').val();
                        output = XenForo.htmlspecialchars(output);

                        output = output
                            .replace(/\t/g, '	')
                            .replace(/ /g, '&nbsp;')
                            .replace(/\n/g, '\n<br/>');

                        ed.restoreSelection();
                        ed.execCommand('inserthtml', output);
                    });
                }, ed));
            };

            if(typeof config.buttonsCustom != undefined && typeof config.buttonsCustom.custom_demo != undefined){
                var eptButton = config.buttonsCustom.custom_demo;

                eptButton.callback = eptButtonCallback;
            }

            RedactorPlugins.klEpt = {
                init: function() {
                    var eptBtnId = 'kl_ept';

                    this.addBtn(
                        eptBtnId,
                        editorFramework.getText('kl_ept_title'),
                        this.eptBtnCallback
                    );
                    this.eptBtnFormatLayout(eptBtnId);
                },
                eptBtnCallback: function(ed, ev) {
                    eptButtonCallback(ed, ev);
                },
                eptBtnFormatLayout: function(id) {
                    var $toolbar = this.$toolbar,
                        $eptBtn = $toolbar.find('.redactor_btn_' + id);

                    $eptBtn
                        .appendTo($toolbar) 
                        .wrap('<li class="redactor_btn_group redactor_btn_right">')
                        .wrap('<ul>')
                        .wrap('<li class="redactor_btn_container_' + id + '">');
                }
            };

            if(typeof config.plugins === undefined || !$.isArray(config.plugins)){
                config.plugins = [];
            }

            config.plugins.push('klEpt');
        });
    }
}
(jQuery, this, document, 'undefined');